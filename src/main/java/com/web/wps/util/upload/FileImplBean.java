package com.web.wps.util.upload;

import com.web.wps.propertie.UploadProperties;
import com.web.wps.util.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class FileImplBean {
    @Resource
    UploadProperties uploadProperties;

    public FileInterface fileInterface(){
        return (FileInterface) BeanUtils.get(uploadProperties.getFileLocation());
    }
}
